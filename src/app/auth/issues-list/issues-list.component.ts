import { Component, OnInit } from '@angular/core';
import { IssuesListService } from './services/issues-list.services';
import { Issue } from './models/issue.model';

@Component( {
  selector: 'app-issues-list',
  templateUrl: './issues-list.component.html',
  styleUrls: [ './issues-list.component.css' ]
} )
export class IssuesListComponent implements OnInit {
  isLoading = true;
  issues: Array<Issue>;

  constructor( private  _issuesListServices: IssuesListService ) { }

  ngOnInit() {
    this.getAllIssues();
  }

  getAllIssues() {
    this._issuesListServices.getAll().subscribe(
      ( data: Issue[] ) => {
        // next
        this.issues = data;
        this.isLoading = false;
      },
      err => {
        console.error( err );
      },
      () => {
        console.log( 'Finished!!' );
      }
    );
  }


  onDeleteIssue( issue: Issue ) {
    // http://localhost:8085/projects/1
    this._issuesListServices.deleteIssue( issue ).subscribe( ( data ) => {
      console.log( data );
      this.getAllIssues();
    } );
  }

  public setData( sortedIssues ) {
    console.log( 'sortedData: %o', sortedIssues );
    this.issues = sortedIssues;
  }

}
