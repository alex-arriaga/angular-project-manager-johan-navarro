import { Component, OnInit } from '@angular/core';
//importamos la class ProjectListService del archivo
//project-list.services.ts con el mismo nombre
import {ProjectListService} from "./services/project-list.services";
import {Project} from './models/project.model';
@Component({
  selector: 'app-project-list',
  templateUrl: './project-list.component.html',
  styleUrls: ['./project-list.component.css']
})
export class ProjectListComponent implements OnInit {
  isLoading = true;
  projects: Array<Project>;
  //injectamos el servicio que queremos consumir y lo importamos
  constructor(private  _projectListServices: ProjectListService) { }
  
  /*
    * Este metodo se ejecuta hasta que los componentes esta listos par ser
    * Utilizados
    * Este tbn es el observable
  */
  ngOnInit() {
    this.getAllProjects();
  }
  getAllProjects(){
    this._projectListServices.getAll().subscribe(
      (data: Project[])=>{
        //next
        this.projects=data;
        this.isLoading=false;//luego de recibir todos los proyectos se detine el loader
      },
      err =>{
        console.error(err);
      },
      ()=>{
        console.log('Finished!!');
      }
  
    );
  }
 
  onDeleteProject(project : Project){
    this._projectListServices.deleteProject(project).subscribe((data)=>{
     console.log(data);
     this.getAllProjects();
    });
    
  }
  public setData(sortedProject) {
    console.log('sortedData: %o', sortedProject);
    this.projects = sortedProject;
  }
  
}
